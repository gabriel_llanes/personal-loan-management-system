package com.digisafari.plms.zuul.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.digisafari.plms.zuul.filter.PostFilter;
import com.digisafari.plms.zuul.filter.PreFilter;
import com.digisafari.plms.zuul.filter.RouterFilter;

@Configuration
public class BeanConfiguration {

	@Bean
	  public PreFilter preFilter(){
	    return new PreFilter();
	  }

	  @Bean
	  public PostFilter postFilter(){
	    return new PostFilter();
	  }

	  public RouterFilter routerFilter(){
	    return new RouterFilter();
	  }
	
}
