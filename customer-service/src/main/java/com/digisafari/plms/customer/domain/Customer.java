package com.digisafari.plms.customer.domain;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "customers")
public class Customer {
    
	@Id
	private String id;    
	private String name;
	private String email;
	private String password;
	private int contactNo;	
	private String profilePicUrl;
	private String gender;
	private int age;
	private String company;
	private String designation;
	private int monthlySalary;
	private int creditScore;	
	private List<CustomerLoan> appliedLoans;
	private List<Loan> favLoans;
	
	public Customer() {
		this.appliedLoans = new ArrayList<>();
		this.favLoans = new ArrayList<>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getContactNo() {
		return contactNo;
	}

	public void setContactNo(int contactNo) {
		this.contactNo = contactNo;
	}

	

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public int getMonthlySalary() {
		return monthlySalary;
	}

	public void setMonthlySalary(int monthlySalary) {
		this.monthlySalary = monthlySalary;
	}

	public int getCreditScore() {
		return creditScore;
	}

	public void setCreditScore(int creditScore) {
		this.creditScore = creditScore;
	}

	public String getProfilePicUrl() {
		return profilePicUrl;
	}

	public void setProfilePicUrl(String profilePicUrl) {
		this.profilePicUrl = profilePicUrl;
	}

	public List<CustomerLoan> getAppliedLoans() {
		return appliedLoans;
	}

	public void setAppliedLoans(List<CustomerLoan> appliedLoans) {
		this.appliedLoans = appliedLoans;
	}

	public List<Loan> getFavLoans() {
		return favLoans;
	}

	public void setFavLoans(List<Loan> favLoans) {
		this.favLoans = favLoans;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", name=" + name + ", email=" + email + ", password=" + password + ", contactNo="
				+ contactNo + ", profilePicUrl=" + profilePicUrl + ", gender=" + gender + ", age=" + age + ", company="
				+ company + ", designation=" + designation + ", montlySalary=" + monthlySalary + ", creditScore="
				+ creditScore + ", appliedLoans=" + appliedLoans + ", favLoans=" + favLoans + "]";
	}

	
	
	
	
	
	
	
	

}
