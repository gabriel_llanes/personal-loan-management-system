package com.digisafari.plms.customer.service;

import java.io.IOException;
import java.util.List;

import com.digisafari.plms.customer.domain.Customer;
import com.digisafari.plms.customer.domain.CustomerLoan;
import com.digisafari.plms.customer.domain.Loan;
import com.digisafari.plms.customer.exception.CustomerAlreadyExistsException;
import com.digisafari.plms.customer.exception.CustomerLoanNotFoundException;
import com.digisafari.plms.customer.exception.CustomerNotFoundException;
import com.digisafari.plms.customer.exception.LoanAlreadyAppliedException;
import com.digisafari.plms.customer.exception.LoanAlreadyFavouritedException;
import com.digisafari.plms.customer.exception.LoanNotFoundException;

public interface ICustomerService {
	
	public List<Customer> getCustomers();
	public Customer getCustomerById(String id) throws CustomerNotFoundException;
	public Customer updateCustomer(Customer customer) throws CustomerNotFoundException;
	public List<CustomerLoan> getCustomerLoans(String customerId) throws CustomerNotFoundException;
	public Customer signupCustomer(Customer customer) throws CustomerAlreadyExistsException, IOException;
	public boolean checkCustomerAlreadyExistsWithEmail(String email) ;
	public List<Loan> getCustomerFavLoans(String customerId) throws CustomerNotFoundException;
	public Loan addLoanToCustomerFavList(String customerId,Loan loan) throws CustomerNotFoundException,LoanAlreadyFavouritedException;
	public boolean removeLoanFromCustomerFavList(String customerId, Loan loan) throws CustomerNotFoundException,LoanNotFoundException;
    public CustomerLoan checkCustomerAlreadyAppliedForLoan(String loanId, String customerId) throws LoanAlreadyAppliedException,CustomerNotFoundException;
	public CustomerLoan approveCustomerLoan(String customerId, String loanId) throws CustomerNotFoundException,CustomerLoanNotFoundException;
	public CustomerLoan rejectCustomerLoan(String customerId, String loanId) throws CustomerNotFoundException,CustomerLoanNotFoundException;
	public CustomerLoan applyNewLoan(String customerId,CustomerLoan customerLoan) throws CustomerNotFoundException,LoanAlreadyAppliedException;
	
	

}
