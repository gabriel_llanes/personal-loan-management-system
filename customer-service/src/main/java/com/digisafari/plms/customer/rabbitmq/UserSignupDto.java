package com.digisafari.plms.customer.rabbitmq;

public class UserSignupDto {
	
	private String custId;
	private String name;
	private String email;
	private String password;
	private String role;
	
	public UserSignupDto() {
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "UserSignupDto [custId=" + custId + ", name=" + name + ", email=" + email + ", password=" + password
				+ ", role=" + role + "]";
	}

	
}
