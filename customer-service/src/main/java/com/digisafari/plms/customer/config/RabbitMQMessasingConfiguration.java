package com.digisafari.plms.customer.config;

import javax.annotation.PostConstruct;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class RabbitMQMessasingConfiguration {

	@Value("${userExchange}")
	private String userExchange ;
	@Value("${userRegisterQueue}")
	private String userRegisterQueue ;
	@Value("${userUpdateQueue}")
	private String userUpdateQueue ;
	@Value("${loansQueue}")
	private String loansQueue ;
	@Value("${userRegisterKey}")
	private String userRegisterKey;
	@Value("${userUpdateKey}")
	private String userUpdateKey;
	@Value("${loansKey}")
	private String loansKey;

	

	  @Autowired
		private AmqpAdmin amqpAdmin;
	
	@Bean
	  DirectExchange exchange(){
	    return new DirectExchange(userExchange);

	  }

	  @Bean
	  Queue userRegisterQueue(){
	    return new Queue(userRegisterQueue , true,false,false,null);
	  }
	  
	  @Bean
	  Queue userUpdateQueue(){
	    return new Queue(userUpdateQueue , true,false,false,null);
	  }
	  
	  @Bean
	  Queue loanQueue(){
	    return new Queue(loansQueue ,  true,false,false,null);
	  }
	  
	  @Bean
	  Binding bindingRegisterQueue(Queue userRegisterQueue , DirectExchange exchange){
	    return BindingBuilder.bind(userRegisterQueue()).to(exchange).with(userRegisterKey);
	  }

	  @Bean
	  Binding bindingUserUpdateQueue(Queue userUpdateQueue , DirectExchange exchange){
	    return BindingBuilder.bind(userUpdateQueue()).to(exchange).with(userUpdateKey);
	  }

	  
	  @Bean
	  Binding bindingLoanQueue(Queue loanQueue , DirectExchange exchange){
	    return BindingBuilder.bind(loanQueue()).to(exchange).with(loansKey);
	  }
	  
	  @Bean
	  public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
	    return new Jackson2JsonMessageConverter();
	  }
	  

	   
	    @PostConstruct
	    public void createQueues() {
	        amqpAdmin.declareQueue(new Queue(userRegisterQueue, true));
	        amqpAdmin.declareQueue(new Queue(userUpdateQueue, true));
	        amqpAdmin.declareQueue(new Queue(loansQueue, true));
	    }

	  @Bean
	  public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
	    RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
	    rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());
	    return rabbitTemplate;
	  }


}
