package com.digisafari.plms.customer.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT, reason = "Loan Already Applied")
public class LoanAlreadyAppliedException extends Exception {

}
