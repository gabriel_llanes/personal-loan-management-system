package com.digisafari.plms.customer.config;

import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.digisafari.plms.customer.domain.Loan;
import com.digisafari.plms.customer.rabbitmq.UserSignupDto;

@Component
public class Producer {

	@Autowired
	private RabbitTemplate rabbitMQTemplate;
	
	@Autowired
	private DirectExchange directExchange;
	
	@Value("${userRegisterKey}")
	private String userRegisterKey;
	@Value("${userUpdateKey}")
	private String userUpdateKey;
	@Value("${loansKey}")
	private String loansKey;
	
	public void sendUserSignUpDetailsToRabbitMQ(UserSignupDto userSignupDto) {
		rabbitMQTemplate.convertAndSend(directExchange.getName(),userRegisterKey,userSignupDto);
	}
	public void sendUserUpdatedDetailsToRabbitMQ(UserSignupDto userSignupDto) {
		rabbitMQTemplate.convertAndSend(directExchange.getName(),userUpdateKey,userSignupDto);
	}
	
	public void sendLoanDetailsToRabbitMQ(Loan loan) {
		rabbitMQTemplate.convertAndSend(directExchange.getName(),loansKey,loan);
	}
	
	
}
