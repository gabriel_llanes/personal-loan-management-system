package com.digisafari.plms.customer.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code= HttpStatus.NOT_FOUND, reason = "Customer doesn't exist with this email")
public class CustomerNotFoundException extends Exception {

}
