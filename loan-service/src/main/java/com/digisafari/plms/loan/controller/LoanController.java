package com.digisafari.plms.loan.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.digisafari.plms.loan.domain.Loan;
import com.digisafari.plms.loan.exception.LoanAlreadyExistsException;
import com.digisafari.plms.loan.service.LoanService;

@RestController
@CrossOrigin("*")
@RequestMapping("api/v1/loans")
public class LoanController {
	
	@Autowired
	private LoanService loanService;
	
	private ResponseEntity<?> responseEntity;
	
	
	@PostMapping("/loans")	
	public ResponseEntity<?> saveLoan(@RequestBody Loan loan) throws LoanAlreadyExistsException{	
		try {
			Loan createdLoan = this.loanService.saveLoan(loan);
			responseEntity = new ResponseEntity<>(createdLoan, HttpStatus.CREATED);
		}		
		catch (LoanAlreadyExistsException e) {
			throw e;
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return responseEntity;
	}
	
	@GetMapping("/loans")
	public ResponseEntity<?> getLoans() {
		List<Loan> loansList = null;
		try {
			loansList = this.loanService.getLoansList();
			responseEntity = new ResponseEntity<>(loansList, HttpStatus.OK);
		}		
		catch (Exception e) {
			e.printStackTrace();
			responseEntity = new ResponseEntity<>("Some internal error occured. Please try again..", HttpStatus.INTERNAL_SERVER_ERROR);
		}		
		return responseEntity;
	}
	
	

}
