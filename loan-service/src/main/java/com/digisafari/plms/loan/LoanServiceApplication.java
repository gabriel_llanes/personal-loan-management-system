package com.digisafari.plms.loan;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import com.digisafari.plms.loan.domain.Loan;
import com.digisafari.plms.loan.repository.LoanRepository;
import com.digisafari.plms.loan.service.LoanService;

@SpringBootApplication
@EnableEurekaClient
public class LoanServiceApplication implements CommandLineRunner {
	
	@Autowired
	ResourceLoader resourceLoader;
	
	@Autowired
	LoanService loanService;
	
	@Autowired
	LoanRepository loanRepository;

	public static void main(String[] args) {
		SpringApplication.run(LoanServiceApplication.class, args);
		
		
	}

	@Override
	public void run(String... args) throws Exception {
		Resource resource = resourceLoader.getResource("classpath:loans.csv");
		try(BufferedReader br = new BufferedReader(new InputStreamReader(resource.getInputStream()))){
			String loanToBeAdded = "";
			loanToBeAdded = br.readLine();
			while(loanToBeAdded != null) {
				String[] loanDetails = loanToBeAdded.split(",");
				Loan loan = new Loan();
				loan.setId(loanDetails[0]);
				loan.setBankName(loanDetails[1]);
				loan.setBankImageUrl(loanDetails[2]);
				loan.setMinLoanAmount(Integer.parseInt(loanDetails[3]));
				loan.setMaxLoanAmount(Integer.parseInt(loanDetails[4]));
				loan.setMinInterestRate(Float.parseFloat(loanDetails[5]));
				loan.setMaxInterestRate(Float.parseFloat(loanDetails[6]));
				loan.setMinCreditScore(Integer.parseInt(loanDetails[7]));
				loan.setTermLength(Byte.parseByte(loanDetails[8]));
				loan.setProcessingFee(Integer.parseInt(loanDetails[9]));
				loan.setRating(Float.parseFloat(loanDetails[10]));
				loan.setFavouriteCount(Integer.parseInt(loanDetails[11]));
				
				System.out.println("Loan-->" + loan);
				Optional<Loan> loanByIdOptional = this.loanRepository.findById(loan.getId());
				if(!loanByIdOptional.isPresent())
				loanService.saveLoan(loan);
				loanToBeAdded = br.readLine();
			}
		}
	    
	    
		
	}
	
	/* Reading loans from loans.csv and store it in database
	 * */
	
	

}
