package com.digisafari.plms.loan.service;

import java.util.List;

import com.digisafari.plms.loan.domain.Loan;
import com.digisafari.plms.loan.exception.LoanAlreadyExistsException;
import com.digisafari.plms.loan.exception.LoanNotFoundException;

public interface ILoanService {
	
	public Loan getLoanById(String loanId) throws LoanNotFoundException;
	public List<Loan> getLoansList();
	public Loan updateLoan(Loan loan) throws LoanNotFoundException;
	public Loan saveLoan(Loan loan) throws LoanAlreadyExistsException;
	

}
