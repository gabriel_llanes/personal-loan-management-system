import { NgModule, OnInit } from "@angular/core";
import { CommonModule } from "@angular/common";

import { CustomersRoutingModule } from "./customers-routing.module";
import { CustomersComponent } from "./customers.component";
import { LoginComponent } from "./components/login/login.component";
import { SignupComponent } from "./components/signup/signup.component";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from '../angular-material/angular-material.module';

@NgModule({
  declarations: [CustomersComponent, LoginComponent, SignupComponent],
  imports: [CommonModule, CustomersRoutingModule, FormsModule,ReactiveFormsModule,AngularMaterialModule]
})
export class CustomersModule {
  constructor() {
    console.log("Inside CustomerModule");
  }
}
