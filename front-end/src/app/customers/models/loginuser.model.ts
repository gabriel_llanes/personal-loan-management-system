export class LoginUser{
    public id: number;
    public custId: number;
    public name : string;
    public email: string;
    public password: string;
    public role : string;
}