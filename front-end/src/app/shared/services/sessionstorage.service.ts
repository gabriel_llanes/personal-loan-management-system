import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SessionStorageService {

  public userSessionDataSubject : BehaviorSubject<any>;

  constructor() { 
    this.userSessionDataSubject = new BehaviorSubject({});
  }

  setSessionStorageData(key:string, value :string){
    sessionStorage.setItem(key,value);
  }

  removeSessionStorageData(){
    sessionStorage.removeItem('id');
    sessionStorage.removeItem('email');
    sessionStorage.removeItem('role');
    sessionStorage.removeItem('name');
    sessionStorage.removeItem('token');
    this.userSessionDataSubject.next({});
  }

  getBearerToken(){
    return sessionStorage.getItem('token');
  }

  getSessionStorageData() : BehaviorSubject<any>{
    const items = {...sessionStorage};
    console.log('items', items);
    this.userSessionDataSubject.next(items);
    return this.userSessionDataSubject;


  }
}
