import { Loan } from "./../models/loan.model";
import { Injectable, OnInit } from "@angular/core";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable, throwError, pipe } from "rxjs";
import { catchError,  mergeMapTo, tap, mergeMap } from "rxjs/operators";
import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: "root"
})
export class LoansService implements OnInit {
  
  private readonly LOANS_SERVICE_API_URL: string = environment.loanServiceEndPointUrl;
  

  constructor(private httpClient: HttpClient) {       
  }

  ngOnInit() {}

  
  

  handleErrorCustomerLoans(error: HttpErrorResponse): Observable<any> {
    console.log("Error in Fetching Customer Loans From Backend -->", error);
    let errorMessage = "";
    if (error.error instanceof ErrorEvent) {
      // client-side error
      // errorMessage = `Error: ${error.error.message}`;
      errorMessage = `Some Browser issue occured. Please try again..`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      if (error.status === 404) {
        errorMessage = "Invalid URL. Please try with Proper URL";
      } else if (error.status === 403) {
        errorMessage = "UnAuthorized User. Please try with proper credentials";
      } else {
        errorMessage = "Some internal error occurred. Please try again later..";
      }
    }

    return throwError(errorMessage);
  }

  

  getLoanById(loanId : number) : Observable<Loan> {
    console.log("Loan to be fetched with id" , loanId);
    
    return this.httpClient.get<Loan>(`${this.LOANS_SERVICE_API_URL}/${loanId}`);
  }
  updateLoan(loan : Loan) : Observable<Loan>{
    console.log('Loan to be updated => favoritecount', loan);    
    return this.httpClient.put<Loan>(`${this.LOANS_SERVICE_API_URL}/${loan.id}`,loan);
  }

  incrementLoanFavCount(loan : Loan) {
    console.log("LOan to be incremented", loan);    
 
//  this.getLoanById(loan.id).subscribe(res =>{
//   console.log('loan found', res);     
//  })
    /* this.getLoanById(loan.id).pipe(tap(loanToBeUpdated =>{
     console.log('loan found', loanToBeUpdated);     
     loanToBeUpdated.favouriteCount++;
      this.updateLoan(loanToBeUpdated);
      //return loanToBeUpdated;
    })); */
    return this.updateLoan(loan);
  }
  decrementLoanFavCount(loan : Loan) : Observable<Loan>{
    console.log("loan to be decremented", loan);    
    /*return this.getLoanById(loan.id).pipe(
      tap(loanToBeUpdated =>{
      loanToBeUpdated.favouriteCount--;
      this.updateLoan(loanToBeUpdated);
     // return loanToBeUpdated;
    }));*/
    return this.updateLoan(loan);
 
  }  

  getLoansList(): Observable<Array<Loan>> {
    return this.httpClient
      .get<Array<Loan>>(`${this.LOANS_SERVICE_API_URL}/loans`)
      .pipe(catchError(this.handleErrorCustomerLoans));
  }

  addLoan(loanToBeAdded : Loan): Observable<Loan>{
    return this.httpClient.post<Loan>(`${this.LOANS_SERVICE_API_URL}/loans`, loanToBeAdded).pipe(catchError(this.handleErrorCustomerLoans));
  }
  
}