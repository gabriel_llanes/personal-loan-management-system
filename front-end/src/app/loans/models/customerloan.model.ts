export class CustomerLoan {
  public id : string;
  public bankName : string;
  public bankImageUrl: string;
  public appliedLoanAmount: number;
  public processingFee : number;
  public rateOfInterest: number;
  public termLength: number;
  public creditScore : number;
  public dateOfApplication: string;
  public verificationStatus: string;
  public status: string;

  constructor() {
    this.verificationStatus = "Verification In Progress";
    this.status = "Pending";
  }
}
