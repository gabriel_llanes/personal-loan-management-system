export const environment = {
  production: true,
  customerServiceEndPointUrl : 'http://localhost:8086/customer-service/api/v1/customers',
  loanServiceEndPointUrl: 'http://localhost:8086/loan-service/api/v1/loans',
  authenticationServiceEndPointUrl: 'http://localhost:8086/authentication-service/api/v1/auth'
};
