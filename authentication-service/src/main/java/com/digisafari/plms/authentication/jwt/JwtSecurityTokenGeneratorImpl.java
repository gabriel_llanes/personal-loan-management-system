package com.digisafari.plms.authentication.jwt;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.digisafari.plms.authentication.domain.User;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class JwtSecurityTokenGeneratorImpl implements SecurityTokenGenerator{

	@Override
	public Map<String, String> generateToken(User user) {
		String jwtToken = null;
	    jwtToken = Jwts.builder().setSubject(user.getEmail()).setIssuedAt(new Date())
	      .signWith(SignatureAlgorithm.HS256,"secretkey").compact();

	    Map<String,String> map = new HashMap<>();
	    map.put("id", user.getCustId());
	    map.put("name", user.getName());
	    map.put("token",jwtToken);
	    
	    map.put("message", "User Successfully logged in");
	    map.put("role", user.getRole());
	    return map;
	  }
	}
	
	


