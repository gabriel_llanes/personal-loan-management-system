package com.digisafari.plms.authentication.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digisafari.plms.authentication.domain.User;
import com.digisafari.plms.authentication.exception.UserNotFoundException;
import com.digisafari.plms.authentication.repository.UserRepository;

@Service
public class UserService implements IUserService {

	@Autowired
	UserRepository userRepository;
	
	@Override
	public User saveUser(User user) {		
		return this.userRepository.save(user);
	}

	@Override
	public User findByEmailAndPassword(String email, String password) throws UserNotFoundException {
		User user = null;
		try {
			user = this.userRepository.findByEmailAndPassword(email, password);
			if(user == null) {
				throw new UserNotFoundException();
			}
		} catch (UserNotFoundException e) {
			throw e;
		}
		return user;
	}

	@Override
	public User updateUser(User user) throws UserNotFoundException {
		User updatedUser = null;
		try {
			User existUser = this.userRepository.findByCustId(user.getCustId());
			if(existUser != null) {
				System.out.println("Exist User ::" + existUser);
				user.setId(existUser.getId());
				updatedUser = this.userRepository.save(user);
			}
		}catch(UserNotFoundException e) {
			throw e;
		}
		return updatedUser;
	}
	
	

}
