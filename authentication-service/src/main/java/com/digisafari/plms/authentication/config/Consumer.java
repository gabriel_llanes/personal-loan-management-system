package com.digisafari.plms.authentication.config;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digisafari.plms.authentication.domain.User;
import com.digisafari.plms.authentication.exception.UserNotFoundException;
import com.digisafari.plms.authentication.service.UserService;

@Component
public class Consumer {

	@Autowired
	UserService userService;
	
	@RabbitListener(queues = "${userRegisterQueue}")
	public void getUserDtoFromRabbitMq(User user) {
		System.out.println("Inside RabbitMQ consumer::" + user);
		this.userService.saveUser(user);
	}
	
	@RabbitListener(queues = "${userUpdateQueue}")
	public void getUserUpdatedDtoFromRabbitMq(User user) throws UserNotFoundException {
		System.out.println("Inside RabbitMQ consumer user update::" + user);
		this.userService.updateUser(user);
	}
}
