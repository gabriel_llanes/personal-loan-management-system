package com.digisafari.plms.authentication.jwt;

import java.util.Map;

import com.digisafari.plms.authentication.domain.User;

public interface SecurityTokenGenerator {
	Map<String, String> generateToken(User user);
}
