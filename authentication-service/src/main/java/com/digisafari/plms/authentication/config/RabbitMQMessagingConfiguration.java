package com.digisafari.plms.authentication.config;

import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQMessagingConfiguration {

	
	 @Bean
	  public MessageConverter jsonMessageConverter() {
	    return new Jackson2JsonMessageConverter();
	  }
}
